/*
 * Created by SharpDevelop.
 * User: casper
 * Date: 15.08.2020
 * Time: 15:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using OpenTK.Platform.Windows;

namespace BlockFlock
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
	double tra_x = 0.0d;
	double tra_y = 2.0d;
	double tra_z = 20.0d;
	int r_leg = 0;
	int y_rotating = 0;
	int z_rotating = 0;
	int animation_walk;
	int hmmm = 0;
	float arm_z = -0.2f;
	float arm2_z = -0.08f;
	float walk_char = 0;
	float x_char = 0;
	double gravity = 0.05;
	 int tex;
	bool jump = false;
	bool fall = false;
	bool wireframe_solid = false;
	bool on_ground = true;
	bool game_over = false;
	bool stopupdate = false;
	float move_x = 0;
	int value_j = 0;
	bool fall2 = false;
	double jitter_y = 0;
	int val_ = 0;
	int ooof = 0;
	bool debugmdl = false;
	int frame = 0;
	bool game_started= false;
	bool cutscene_1 = false;
	bool post_process = false;
	bool test_move = true;
	bool ram_benchmark = true;
	Bitmap light = new Bitmap("fx/light.png");
	Bitmap effect2 = new Bitmap("fx/detail.png");  
	static Bitmap floor = new Bitmap(400,400,System.Drawing.Imaging.PixelFormat.Format24bppRgb);
	static Bitmap floor2 = new Bitmap("textures/floor.png");
	static Bitmap test_shadow = new Bitmap("shadow_maps/test_map1.png");
	static Bitmap floor_texturetest = new Bitmap(400,400,System.Drawing.Imaging.PixelFormat.Format24bppRgb);
	Graphics floor_light = Graphics.FromImage(floor);
	Graphics floor_test = Graphics.FromImage(floor_texturetest);
	BitmapData data2;
	string debug_models = "test";
	 float[] blue= {0.0f,0.0f, 1.0f, 0.0f};
	 int floor_texture;
	 
	 
	 byte[] halftone = {
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55};
	 
	 byte[] concrete  = { 0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0x55 ,0x55 ,0x55 ,0x55 ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0x55 ,0x55 ,0x55 ,0x55 ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff,
0xff,0xff,0xff ,0xff ,0xff ,0xff ,0xff ,0xff	 };
	 
		string leg = "begin_quads nx 0,5456 0,6010 0,5840 v -0,023013 -3,189971 0,321423 nx 0,5676 0,6010 -0,5626 v -0,038606 -3,189971 1,127007 nx -0,5573 0,5766 -0,5974 v 0,843066 -3,189971 0,321423 end begin_quads nx -0,5798 0,5766 0,5756 v 0,843066 -3,189971 0,321423 nx -0,9066 0,3308 0,2618 v 0,683045 -1,421751 0,323062 -0,7107 nx -0,7027 0,0323 -0,7107 v 0,667452 -1,421751 1,128461 nx 0,7205 0,0000 -0,6934 v 0,827473 -3,189971 1,127007 end begin_quads nx 0,5883 -0,5773 -0,5661 v -0,033544 -1,371199 0,300999 nx 0,7085 -0,7054 0,0188 v -0,029552 -3,189968 0,300999 nx -0,7052 -0,7088 -0,0187 v 0,835196 -3,189971 0,317848 nx -0,5661 -0,5773 -0,5883 v 0,678864 -1,421751 0,314774 end begin_quad_strip nx 0,8980 0,2985 0,3231 v -0,047421 -1,366844 1,109775 nx -0,5929 0,5766 0,5622 v 0,665396 -1,421751 1,123451 nx 0,5614 0,5781 0,5921 v -0,031919 -1,366844 0,303976 nx 0,5614 -0,5781 0,5921 v 0,680898 -1,421751 0,317652 end begin_quad_strip nx -0,5929 -0,5766 0,5622 v -0,033544 -1,371199 0,300999 v -0,029552 -3,189968 0,300999 v -0,033544 -1,371199 1,110808 v -0,029552 -3,189968 1,110808 end begin_quad_strip nx -0,7052 -0,7088 -0,0187 v 0,667452 -1,421751 1,128461 nx -0,5661 -0,5773 -0,5883 v 0,827473 -3,189971 1,127007 v -0,033544 -1,371199 1,110808 v -0,029552 -3,189968 1,110808 end box 0,4 -3,4 0,4 0,4 0,2 0,8";
		string torso = "begin_quad_strip nx -0,0000 0,1675 0,9859 v 0,614892 0,821023 1,130603 v 0,729491 -1,274505 1,486679 v -0,960411 0,821023 1,130603 v -1,075010 -1,274505 1,486679 end begin_quad_strip nx 0,9985 0,0546 -0,0000 v 0,614892 0,821023 0,338304 v 0,729491 -1,274505 -0,017772 v 0,614892 0,821023 1,130603 v 0,729491 -1,274505 1,486679 end begin_quad_strip nx -0,0000 1,0000 -0,0000 v 0,614892 0,821023 0,338304 v 0,614892 0,821023 1,130603 v -0,960411 0,821023 0,338304 v -0,960411 0,821023 1,130603 end begin_quad_strip nx -0,9985 0,0546 -0,0000 v -0,960411 0,821023 0,338304 v -1,075010 -1,274505 -0,017772 v -0,960411 0,821023 1,130603 v -1,075010 -1,274505 1,486679 end begin_quad_strip nx -0,0000 -1,0000 0,0000 v 0,729491 -1,274505 -0,017772 v 0,729491 -1,274505 1,486679 v -1,075010 -1,274505 -0,017772 v -1,075010 -1,274505 1,486679 end begin_quad_strip nx 0,0000 0,1675 -0,9859 v 0,614892 0,821023 0,338304 v 0,729491 -1,274505 -0,017772 v -0,960411 0,821023 0,338304 v -1,075010 -1,274505 -0,017772 end";
		string arm = "begin_quad_strip nx 0,0125 0,1294 0,9915 v 0,805218 -1,344586 1,244281 v 0,682954 0,891104 0,964819 v 1,666350 -1,345963 1,244474 v 1,276654 0,751857 0,964668 end begin_quad_strip nx 0,9832 0,1826 -0,0000 v 1,666350 -1,345963 1,244474 v 1,276654 0,751857 0,964668 v 1,666350 -1,345964 0,224433 v 1,276654 0,751857 0,504239 end begin_quad_strip nx 0,2283 0,9736 0,0000 v 0,682954 0,891104 0,964819 v 0,682954 0,891104 0,504087 v 1,276654 0,751857 0,964668 v 1,276654 0,751857 0,504239 end begin_quad_strip nx -0.0016 -1.0000 0.0000 v 0,805218 -1,344586 0,224626 v 0,682954 0,891104 0,504087 v 1,666350 -1,345964 0,224433 v 1,276654 0,751857 0,504239 end begin_quad_strip nx 0,0125 0,1294 -0,9915 v 0,805218 -1,344586 1,244281 v 0,682954 0,891104 0,964819 v 0,805218 -1,344586 0,224626 v 0,682954 0,891104 0,504087 end begin_quad_strip nx -0,0016 -1,0000 0,0000 v 0,805218 -1,344586 1,244281 v 0,805218 -1,344586 0,224626 v 1,666350 -1,345963 1,244474 v 1,666350 -1,345964 0,224433 end";
		string model1 = "begin_quad_strip nx -0,0000 0,0000 1,0000 tex 1,0 0,0 v -2,078323 0,000000 -3,968553 tex 1,0 1,0 v 2,078323 0,000000 -3,968553 tex 0,0 1,0 v -2,078323 4,538413 -3,968553 tex 0,0 0,0 v 2,078323 4,470997 -3,968553 end begin_quad_strip nx -1,0000 0,0000 0,0000 v 2,078323 0,000000 3,968553 v 2,078323 0,000000 -3,968553 v 2,078323 4,470997 3,968553 v 2,078323 4,470997 -3,968553 end begin_quad_strip nx -0,6783 -0,7347 0,0000 v 2,078323 4,470997 3,968553 v 2,078323 4,470997 -3,968553 v -0,043602 6,430069 3,968553 v -0,043602 6,430069 -3,968553 end begin_tri nx 0,0000 0,0000 1,0000 v -2,078323 4,538413 -3,968553 v 2,078323 4,470997 -3,968553 v -0,043602 6,430069 -3,968553 end begin_quad_strip nx 0,6809 -0,7324 0,0000 v -2,078323 4,538413 3,968553 v -2,078323 4,538413 -3,968553 v -0,043602 6,430069 3,968553 v -0,043602 6,430069 -3,968553 end begin_quad_strip nx 1,0000 0,0000 0,0000 v -2,078323 0,000000 3,968553 v -2,078323 0,000000 -3,968553 v -2,078323 4,538413 3,968553 v -2,078323 4,538413 -3,968553 end begin_quad_strip nx 0,0000 1,0000 0,0000 v -2,078323 0,000000 3,968553 v 2,078323 0,000000 3,968553 v -2,078323 0,000000 -3,968553 v 2,078323 0,000000 -3,968553 end begin_quad_strip v -2,078323 0,000000 3,968553 v 2,078323 0,000000 3,968553 v -2,078323 4,538413 3,968553 v 2,078323 4,470997 3,968553 end begin_tri v -2,078323 4,538413 3,968553 v 2,078323 4,470997 3,968553 v -0,043602 6,430069 3,968553 end";
		string model2 = "begin_quad_strip nx 0,0000 1,0000 0,0000 tex 1 0 v -7,849617 -1,666737 10,107405 tex 1 1 v 7,849617 -1,666737 10,107405 tex 0 1 v -7,849617 -1,666737 24,851604 tex 0 0 v 7,849617 -1,666737 24,851604 end begin_quad_strip nx 0,0000 0,6901 0,7237 tex 1 0 v -7,849617 0,000000 8,517921 tex 1 1 v 7,849617 0,000000 8,517921 tex 0 1 v -7,849617 -1,666737 10,107405 tex 0 1 v 7,849617 -1,666737 10,107405 end begin_quad_strip nx 0,0000 1,0000 0,0000 tex 1 0 v -7,849617 0,000000 8,517921 tex 1 1 v 7,849617 0,000000 8,517921 tex 0 1 v -7,849617 0,000000 -8,517921 tex 0 0 v 7,849617 0,000000 -8,517921 end begin_quad_strip nx 0,0000 0,8185 0,5745 tex 1 0 v -7,849617 0,000000 -8,517921 tex 1 1 v 7,849617 0,000000 -8,517921 tex 0 1 v -7,849617 1,211514 -10,244198 tex 0 0 v 7,849617 1,211514 -10,244198 end begin_quad_strip nx 0,0000 1,0000 0,0000 tex 1 0 v -7,849617 1,211514 -10,244198 tex 1 1 v 7,849617 1,211514 -10,244198 tex 0 1 v -7,849617 1,211514 -37,947479 tex 0 0 v 7,849617 1,211514 -37,947479 end begin_quad_strip nx 0,0000 0,5474 0,8369 tex 1 0 v -7,849617 1,211514 -37,947479 tex 1 1 v 7,849617 1,211514 -37,947479 tex 0 1 v -7,849617 4,374489 -40,016293 tex 0 0 v 7,849617 4,374489 -40,016293 end begin_quad_strip nx 0,0000 1,0000 0,0000 tex 1 0 v -7,849617 4,374489 -40,016293 tex 1 1 v 7,849617 4,374489 -40,016293 tex 0 1 v -7,849617 4,374489 -55,170940 tex 0 0 v 7,849617 4,374489 -55,170940 end";
		void GlControl1Load(object sender, EventArgs e)
		{
				   base.OnLoad(e);
				   
				     GL.Enable(EnableCap.DepthTest);
//				     GL.Enable(EnableCap.Fog);
// GL.Fog(FogParameter.FogMode,Convert.ToSingle(FogMode.Linear)); 
 //GL.Fog(FogParameter.FogColor,blue);
 //GL.Fog(FogParameter.FogEnd,100);

 	GL.LineWidth(2);
				               GlControl1Resize(this, EventArgs.Empty);   // Ensure the Viewport is set up correctly
     //      GL.ClearColor(Color.Crimson);

     GL.ClearColor(Color.DarkGreen);
     
     for(int j = 0; j < 9600; j+=24){
     	     for(int y_coord = 0; y_coord < 3600; y_coord+=90){
     		 	floor_test.DrawImage(floor2,j,y_coord,24,90);
     	}
    
     }
     

     floor_light.DrawImage(light,0,0);
     floor_light.DrawImage(test_shadow,move_x,0);
     
     floor_light.Clear(Color.Black);
			
			
			floor_light.DrawImage(floor_texturetest,0,0);
			
			     floor_light.DrawImage(test_shadow,move_x*12,0);
			     

      
    GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

    GL.GenTextures(1, out tex);
    GL.BindTexture(TextureTarget.Texture2D, tex);

    BitmapData data = effect2.LockBits(new System.Drawing.Rectangle(0, 0, effect2.Width, effect2.Height),
        ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

    GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
        OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
    effect2.UnlockBits(data);

     GL.GenTextures(1, out floor_texture);
    GL.BindTexture(TextureTarget.Texture2D, floor_texture);

    
  
    data2 = floor.LockBits(new System.Drawing.Rectangle(0, 0,floor.Width, floor.Height),
        ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data2.Width, data2.Height, 0,
                     OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data2.Scan0);
    
  //  GL.TexSubImage2D(TextureTarget.Texture2D,0,0,0,data2.Width,data2.Height,OpenTK.Graphics.OpenGL.PixelFormat.Rgba,PixelType.UnsignedByte,data2.Scan0);
    	//GL.TexImage2D(
    //GL.TexImage2D();
    floor.UnlockBits(data2);
    
            /*   GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

    GL.GenTextures(1, out floor_texture);
    GL.BindTexture(TextureTarget.Texture2D, floor_texture);

    
  
    data2 = floor.LockBits(new System.Drawing.Rectangle(0, 0,floor.Width, floor.Height),
        ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

    GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data2.Width, data2.Height, 0,
        OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data2.Scan0);
    floor.UnlockBits(data2);
    
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int) TextureWrapMode.Repeat);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int) TextureWrapMode.Repeat);
    */
    

    
       //int tex;
   
		}
		
			      void Cube(float x,float y,float z,float size_x,float size_y,float size_z){
        	
        	{
            	 	     	GL.PushMatrix();
        	GL.Translate(x,y,z);
        	GL.Scale(size_x,size_y,size_z);
GL.Begin(PrimitiveType.Quads);

//GL.Normal3(0.0000,1.0000,0.0000);
    // Front Face
     GL.Normal3(-0.5773,0.5773,-0.5773);
    GL.TexCoord2(0.0f, 0.0f); GL.Vertex3(-1.0f, -1.0f,  1.0f);  // Bottom Left Of The Texture and Quad
    GL.TexCoord2(1.0f, 0.0f);  GL.Vertex3( 1.0f, -1.0f,  1.0f);  // Bottom Right Of The Texture and Quad
    GL.TexCoord2(1.0f, 1.0f);   GL.Vertex3( 1.0f,  1.0f,  1.0f);  // Top Right Of The Texture and Quad
    GL.TexCoord2(0.0f, 1.0f);   GL.Vertex3(-1.0f,  1.0f,  1.0f);  // Top Left Of The Texture and Quad
    // Back Face
    GL.Normal3(0.0000,1.0000,0.0000);
  
    GL.TexCoord2(1.0f, 0.0f); GL.Vertex3(-1.0f, -1.0f, -1.0f);  // Bottom Right Of The Texture and Quad
    GL.TexCoord2(1.0f, 1.0f);GL.Normal3(-0.5773,-0.5773,-0.5773); GL.Vertex3(-1.0f,  1.0f, -1.0f);  // Top Right Of The Texture and Quad
    GL.TexCoord2(0.0f, 1.0f); GL.Vertex3( 1.0f,  1.0f, -1.0f);  // Top Left Of The Texture and Quad
    GL.TexCoord2(0.0f, 0.0f);  GL.Normal3(0.0000,1.0000,0.0000);  GL.Vertex3( 1.0f, -1.0f, -1.0f);  // Bottom Left Of The Texture and Quad
    // Top Face
   
    
    GL.TexCoord2(0.0f, 1.0f); GL.Normal3(0.5773,0.5773,-0.5773); GL.Vertex3(-1.0f,  1.0f, -1.0f);  // Top Left Of The Texture and Quad
    GL.TexCoord2(0.0f, 0.0f); GL.Vertex3(-1.0f,  1.0f,  1.0f);  // Bottom Left Of The Texture and Quad
    GL.TexCoord2(1.0f, 0.0f); GL.Vertex3( 1.0f,  1.0f,  1.0f);  // Bottom Right Of The Texture and Quad
    GL.TexCoord2(1.0f, 1.0f); GL.Vertex3( 1.0f,  1.0f, -1.0f);  // Top Right Of The Texture and Quad
    

    // Bottom Face
    GL.TexCoord2(1.0f, 1.0f); GL.Vertex3(-1.0f, -1.0f, -1.0f);  // Top Right Of The Texture and Quad
    GL.TexCoord2(0.0f, 1.0f); GL.Normal3(-0.5773,-0.5773,-0.5773);   GL.Vertex3( 1.0f, -1.0f, -1.0f);  // Top Left Of The Texture and Quad
    GL.TexCoord2(0.0f, 0.0f); GL.Vertex3( 1.0f, -1.0f,  1.0f);  // Bottom Left Of The Texture and Quad
    GL.TexCoord2(1.0f, 0.0f); GL.Vertex3(-1.0f, -1.0f,  1.0f);  // Bottom Right Of The Texture and Quad
     //  GL.Normal3(0.0000,0.0000,1.0000);
    
    // Right face
    GL.TexCoord2(1.0f, 0.0f); GL.Vertex3( 1.0f, -1.0f, -1.0f);  // Bottom Right Of The Texture and Quad
    GL.TexCoord2(1.0f, 1.0f); GL.Normal3(0.5773,-0.5773,-0.5773); GL.Vertex3( 1.0f,  1.0f, -1.0f);  // Top Right Of The Texture and Quad
    GL.TexCoord2(0.0f, 1.0f); GL.Vertex3( 1.0f,  1.0f,  1.0f);  // Top Left Of The Texture and Quad
    GL.TexCoord2(0.0f, 0.0f); GL.Vertex3( 1.0f, -1.0f,  1.0f);  // Bottom Left Of The Texture and Quad
    // Left Face
 
    
    GL.TexCoord2(0.0f, 0.0f); GL.Vertex3(-1.0f, -1.0f, -1.0f);  // Bottom Left Of The Texture and Quad
    GL.TexCoord2(1.0f, 0.0f); GL.Vertex3(-1.0f, -1.0f,  1.0f);  // Bottom Right Of The Texture and Quad
    GL.TexCoord2(1.0f, 1.0f); GL.Vertex3(-1.0f,  1.0f,  1.0f);  // Top Right Of The Texture and Quad
    GL.TexCoord2(0.0f, 1.0f); GL.Vertex3(-1.0f,  1.0f, -1.0f);  // Top Left Of The Texture and Quad
GL.End();        	
GL.PopMatrix();
  //GL.Light(LightName.Light0,LightParameter.Diffuse,light_diffuse);


}}
		
					void model(string model_data){
		  
         
   	 string[] lines = model_data.Split(' '); 
   	  string[] ohherewego ={"wutt" };
   	  string contenta = model_data;
   	//  bool okshit = true;
   	 // if(okshit == true){
   	  //	  foreach (string line in lines)
       // {
            // Use a tab to indent each line of the file.
            string line = "e";
            line = contenta;
             char[] delimiters = new char[] {' ', '\t'};
  
           ohherewego = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
         
           
        int contents2 = contenta.Split(' ').Length/*;*/;

             for (int i = 0; i <  contents2;  /*18*/ /*;*/ i++)
                {
             	 if (ohherewego[i].Contains("begin_tri")){
             		GL.Begin(BeginMode.Triangles);
             	}
             		 if (ohherewego[i].Contains("begin_triangle_strip")){
             		GL.Begin(BeginMode.TriangleStrip);
             	}
             		 if (ohherewego[i].Contains("begin_triangle_fan")){
             		GL.Begin(BeginMode.TriangleFan);
             	}
              	 if (ohherewego[i].Contains("begin_quads")){
             		
             	
             		if(wireframe_solid == true){
             		GL.Begin(BeginMode.LineStrip);
             	
             		GL.Disable(EnableCap.ColorMaterial);
             		GL.Disable(EnableCap.Lighting); 
             		
             		GL.Color3(Color.Black);
             		if(ohherewego[i+1].Contains("nx")){
             			
             			GL.Normal3(Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]),Single.Parse(ohherewego[i+4]));
             			}else{
             		   	
             		   	GL.Vertex3(Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]),Single.Parse(ohherewego[i+4]));
             		   }
             		GL.Vertex3(Single.Parse(ohherewego[i+6]),Single.Parse(ohherewego[i+7]),Single.Parse(ohherewego[i+8]));
             		GL.Vertex3(Single.Parse(ohherewego[i+10]),Single.Parse(ohherewego[i+11]),Single.Parse(ohherewego[i+12]));
             		GL.Vertex3(Single.Parse(ohherewego[i+14]),Single.Parse(ohherewego[i+15]),Single.Parse(ohherewego[i+16]));
             		if(ohherewego[i+1].Contains("nx")){
             			GL.Vertex3(Single.Parse(ohherewego[i+18]),Single.Parse(ohherewego[i+19]),Single.Parse(ohherewego[i+20]));
             			}else{
             		   	
             		   }
             		
             		
             		GL.Enable(EnableCap.ColorMaterial);
             		GL.Enable(EnableCap.Lighting); 
             		GL.End();
             		}
             		
             		GL.Begin(BeginMode.Quads);
             	}
             				
            	 if (ohherewego[i].Contains("begin_quad_strip")){
             		
             		if(wireframe_solid == true){
             		GL.Begin(BeginMode.LineStrip);
             	
             		GL.Disable(EnableCap.ColorMaterial);
             		GL.Disable(EnableCap.Lighting); 
             		GL.Disable(EnableCap.DepthTest);
             		GL.Color3(Color.Black);
             					       
 GL.Enable(EnableCap.PolygonStipple);
			        GL.PolygonStipple(halftone);
             		if(ohherewego[i+1].Contains("nx")){
             			
             			//GL.Normal3(Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]),Single.Parse(ohherewego[i+4]));
             			}else{
             		   	
             		   	GL.Vertex3(Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]),Single.Parse(ohherewego[i+4]));
             		   }
             		GL.Vertex3(Single.Parse(ohherewego[i+6]),Single.Parse(ohherewego[i+7]),Single.Parse(ohherewego[i+8]));
             		GL.Vertex3(Single.Parse(ohherewego[i+10]),Single.Parse(ohherewego[i+11]),Single.Parse(ohherewego[i+12]));
             		GL.Vertex3(Single.Parse(ohherewego[i+14]),Single.Parse(ohherewego[i+15]),Single.Parse(ohherewego[i+16]));
             		if(ohherewego[i+1].Contains("nx")){
             			GL.Vertex3(Single.Parse(ohherewego[i+18]),Single.Parse(ohherewego[i+19]),Single.Parse(ohherewego[i+20]));
             			}else{
             		   	
             		   }
             		
             		
             		GL.Enable(EnableCap.ColorMaterial);
             		GL.Enable(EnableCap.Lighting); 
             		GL.Enable(EnableCap.DepthTest);
             		GL.End();
             		GL.Disable(EnableCap.PolygonStipple);
             		}
             		GL.Begin(BeginMode.QuadStrip);
             	}
      
             		if(ohherewego[i].Contains("nx")){
             		
             		GL.Normal3(Single.Parse(ohherewego[i+1]),Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]));
            		//GL.Normal3(Single.Parse(ohherewego[i+1]),Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]));
            	}
 	if(ohherewego[i].Contains("tex")){
             		
             		GL.TexCoord2(Single.Parse(ohherewego[i+1]),Single.Parse(ohherewego[i+2]));
            		//GL.Normal3(Single.Parse(ohherewego[i+1]),Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]));
            	}
             	
             	 if (ohherewego[i].Contains("v")){
             		
             		
             		
             	//	GL.TexCoord2(0.0f,0.0f);
             		
             		GL.Vertex3(Single.Parse(ohherewego[i+1]),Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]));
             		//GL.Vertex3(Single.Parse(ohherewego[i+4]),Single.Parse(ohherewego[i+5]),Single.Parse(ohherewego[i+6]));
             	//	GL.Vertex3(Single.Parse(ohherewego[i+7]),Single.Parse(ohherewego[i+8]),Single.Parse(ohherewego[i+9]));                                                                                                                                  
             		
             		//Application.Exit();
         
   }  
   
             
              if (ohherewego[i].Contains("box")){
             		
             		Cube(Single.Parse(ohherewego[i+1]),Single.Parse(ohherewego[i+2]),Single.Parse(ohherewego[i+3]),Single.Parse(ohherewego[i+4]),Single.Parse(ohherewego[i+5]),Single.Parse(ohherewego[i+6]));
             }
    if (ohherewego[i].Contains("end")){
             GL.End();		
             }
         	}
	//if (codestore[i].Contains("begin")){
   		
   //	}
    
   	
        //   okshit = false;
       // }
   	 // }
 
   //	  Console.WriteLine(String.Concat(ohherewego));

   
    }
         	
	//if (codestore[i].Contains("begin")){
   		
   //	}
   
  
					
    
				
					        
				//}

     

				void flat_surface(){
					GL.Begin(PrimitiveType.Quads);


    GL.TexCoord2(0.0f, 1.0f);  GL.Normal3(-0.5773,-0.5773,-0.5773); GL.Vertex3(-1.0f,  1.0f, -1.0f);  // Top Left Of The Texture and Quad
    GL.TexCoord2(0.0f, 0.0f); GL.Normal3(0.5773,0.5773,0.5773); GL.Vertex3(-1.0f,  1.0f,  1.0f);  // Bottom Left Of The Texture and Quad
    GL.TexCoord2(1.0f, 0.0f); GL.Vertex3( 1.0f,  1.0f,  1.0f);  // Bottom Right Of The Texture and Quad
    GL.TexCoord2(1.0f, 1.0f);   GL.Normal3(-0.5773,-0.5773,-0.5773); GL.Vertex3( 1.0f,  1.0f, -1.0f);  // Top Right Of The Texture and Quad
    GL.End();	
				}
				
    
		void GlControl1Paint(object sender, PaintEventArgs e)
		{// GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);

			                 
			
						 
            glControl1.MakeCurrent();
 // GL.ClearColor(Color.Crimson);
 
       float[] light_position = {0.0f,5.0f, 4.0f, 0.0f};  /* Infinite light location. */
                  		float[] light_diffuse =  {1.0f, 1.0f, 1.0f, 1.0f};

           GL.Light(LightName.Light0,LightParameter.Diffuse,light_diffuse);
   GL.Light(LightName.Light0,LightParameter.Position,light_position);
               GL.Enable(EnableCap.Light0);
            GL.Enable(EnableCap.Lighting);
            
  
      
    
    
       Matrix4 modelview = Matrix4.LookAt(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref modelview);
            
          
         
          //  GL.Enable(EnableCap.TextureGenS);
           // GL.Enable(EnableCap.TextureGenT);

  

    GL.Enable(EnableCap.Texture2D);
        //    GL.Enable(EnableCap.AlphaTest);
         //   GL.Enable(EnableCap.ColorMaterial);

                     GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                     if(game_started == false){
                     	   GL.Translate(tra_x,tra_y,tra_z);
                     	
                     	    GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);


    GL.BindTexture(TextureTarget.Texture2D, floor_texture);

    
             GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int) TextureWrapMode.Repeat);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int) TextureWrapMode.Repeat);
   /* if(ram_benchmark == true){
           GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

    GL.GenTextures(1, out floor_texture);
    

    
  
    data2 = floor.LockBits(new System.Drawing.Rectangle(0, 0,floor.Width, floor.Height),
        ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
//GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data2.Width, data2.Height, 0,
               //      OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data2.Scan0);
    
   
    	//GL.TexImage2D(
    //GL.TexImage2D();
    floor.UnlockBits(data2);
    
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int) TextureWrapMode.Repeat);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int) TextureWrapMode.Repeat);
    }*/
      GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
          data2 = floor.LockBits(new System.Drawing.Rectangle(0, 0,floor.Width, floor.Height),
        ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
      
    GL.BindTexture(TextureTarget.Texture2D, floor_texture);
      
    GL.TexSubImage2D(TextureTarget.Texture2D,0,0,0,data2.Width,data2.Height,OpenTK.Graphics.OpenGL.PixelFormat.Bgra,PixelType.UnsignedByte,data2.Scan0);
      floor.UnlockBits(data2);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int) TextureWrapMode.Repeat);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int) TextureWrapMode.Repeat);
     	GL.PushMatrix();
                     	GL.Translate(0,2,5);
  	GL.Rotate(90,0,0,1);
                     	GL.Scale(5,0.1,5);
                   
                     	flat_surface();
    GL.PopMatrix();
    
        
     	GL.PushMatrix();
                     	GL.Translate(5,2,5);
  	GL.Rotate(90,0,0,1);
                     	GL.Scale(5,0.1,5);
                   
                     	flat_surface();
    GL.PopMatrix();
                     	//Cube(0,1,5,5,0.1,5);
                     	GL.PushMatrix();
                     	GL.Translate(0,1,5);
                     	GL.Scale(5,0.1,5);

                     	flat_surface();
    GL.PopMatrix();
         
    
    GL.PushMatrix();
    GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);


    GL.BindTexture(TextureTarget.Texture2D, tex);

    Cube(0,0,0,1,2,1);
             GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int) TextureWrapMode.Repeat);
    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int) TextureWrapMode.Repeat);
    
   // GL.Enable(EnableCap.Blend);
   // GL.Disable(EnableCap.DepthTest);
    GL.BlendFunc(BlendingFactorSrc.OneMinusSrcAlpha,BlendingFactorDest.DstAlpha);
    Cube(move_x,4,5,0.81f,1,0.92f);
    //GL.Enable(EnableCap.DepthTest);
   // GL.Disable(EnableCap.Blend);
    
    GL.PopMatrix();
                     }
                     
                     
                     
                     
                     if(cutscene_1 == true){
                     GL.Rotate(hmmm,0,1,0);
                     
                     
                
			        
			        
                     GL.Translate(tra_x,tra_y-10,tra_z+8);
                          
			        GL.PushMatrix();
			        
			        Cube(Convert.ToSingle(-tra_x),Convert.ToSingle(-tra_y+5),Convert.ToSingle(-tra_z+10),1,1,1);
			        GL.PopMatrix();
                     GL.PushMatrix();
                     
                     GL.Translate(0,0,5);
                     GL.Rotate(90,0,1,0);
                     model(model1);
			        GL.PopMatrix();
 GL.PushMatrix();
                     GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Firebrick);
                     GL.Translate(-10,0,5);
                     GL.Rotate(90,0,1,0);
                     model(model1);
			        GL.PopMatrix();			        
			        
			        GL.PushMatrix();
			        GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Gray);
			        GL.Translate(0,0,-5);
			        GL.Rotate(90,0,1,0);
			        model(model2);
			        
			        GL.PopMatrix();
        GL.PushMatrix();
        		GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Green);
			        GL.Translate(0,0,10);
			        GL.Rotate(90,0,1,0);
			        model(model2);
			        
			

			        
			        GL.PopMatrix();			        
			       
			        GL.PushMatrix();
			            GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			            GL.Translate(0,2,-2);
			        GL.Rotate(90,0,1,0);
			        GL.Scale(4,2,2);
			        model("begin_quad_strip nx 0,0000 1,0000 0,0000 v -1,000000 -0,316414 2,025706 v 1,000000 -0,316414 2,025706 nx 0,0000 0,0000 1,0000 v -1,000000 -0,316414 3,281726 v 1,000000 -0,316414 3,281726 end begin_quad_strip nx 0,0000 0,0000 1,0000 v -1,000000 -0,316414 3,281726 v 1,000000 -0,316414 3,281726 nx 1,0000 0,0000 0,0000 v -1,000000 -0,817916 3,281726 v 1,000000 -0,817916 3,281726 end begin_quad_strip nx -1,0000 0,0000 0,0000 v 1,000000 -0,109800 2,023704 v 1,000000 -0,839104 2,025706 nx -1,0000 -0,0000 0,0000 v 1,000000 -0,069150 0,669919 v 1,000000 -0,839104 0,669919 end begin_quad_strip nx -1,0000 -0,0000 0,0000 v 1,000000 -0,105528 0,497482 v 1,000000 -0,109197 -2,502410 nx -1,0000 0,0000 0,0000 v 1,000000 -0,849730 0,500222 v 1,000000 -0,849730 -2,502410 end begin_quad_strip nx -1,0000 0,0000 0,0000 v -0,962688 -0,105528 0,497482 v -0,962688 -0,109197 -2,469042 nx 1,0000 0,0000 0,0000 v -0,962688 -0,849730 0,500222 v -0,962688 -0,849730 -2,469042 end begin_quad_strip nx 1,0000 0,0000 0,0000 v 1,000000 0,701884 1,569230 v 1,000000 -0,316414 2,025706 nx -1,0000 0,0000 0,0000 v 1,000000 0,701884 1,437149 v 1,000000 -0,316414 1,893626 end begin_quads nx -1,0000 0,0000 0,0000 v 1,000000 0,698848 0,671456 v 1,000000 -0,847057 0,671456 nx -1,0000 0,0000 0,0000 v 1,000000 -0,847057 0,500222 v 1,000000 0,691509 0,500222 end begin_quads nx -1,0000 0,0000 0,0000 v 1,000000 0,698848 -0,896936 v 1,000000 -0,847057 -0,896936 nx 0,0000 0,0000 -1,0000 v 1,000000 -0,847057 -1,068169 v 1,000000 0,691509 -1,068169 end begin_quad_strip nx 0,0000 0,0000 -1,0000 v -1,000000 0,701884 -2,643486 v 1,000000 0,701884 -2,643486 nx -1,0000 0,0000 0,0000 v -1,000000 -0,847057 -2,643486 v 1,000000 -0,847057 -2,643486 end begin_quads nx -1,0000 0,0000 0,0000 v 1,000000 0,698848 -2,464993 v 1,000000 -0,847057 -2,464993 nx 0,0000 1,0000 0,0000 v 1,000000 -0,847057 -2,636226 v 1,000000 0,691509 -2,636226 end begin_quad_strip nx 0,0000 1,0000 0,0000 v -1,000000 0,701884 1,569230 v 1,000000 0,701884 1,569230 nx -1,0000 0,0000 0,0000 v -1,000000 0,701884 -2,643486 v 1,000000 0,701884 -2,643486 end begin_quads nx -1,0000 0,0000 0,0000 v -0,962688 0,698848 0,671456 v -0,962688 -0,847057 0,671456 nx -1,0000 0,0000 0,0000 v -0,962688 -0,847057 0,500222 v -0,962688 0,691509 0,500222 end begin_quad_strip nx -1,0000 0,0000 0,0000 v -0,962688 -0,151122 2,025706 v -0,962688 -0,839104 2,025706 nx 1,0000 0,0000 0,0000 v -0,962688 -0,151122 0,670889 v -0,962688 -0,839104 0,670889 end begin_quad_strip nx 1,0000 0,0000 0,0000 v -0,962688 0,701884 1,569230 v -0,962688 -0,316414 2,025706 nx 1,0000 0,0000 0,0000 v -0,962688 0,701884 1,437149 v -0,962688 -0,316414 1,893626 end begin_quad_strip nx 1,0000 0,0000 0,0000 v -0,962688 0,698848 -0,896936 v -0,962688 -0,847057 -0,896936 nx -1,0000 0,0000 0,0000 v -0,962688 -0,847057 -1,068169 v -0,962688 0,691509 -1,068169 end begin_quads nx -1,0000 0,0000 0,0000 v -0,962688 0,698848 -2,464993 v -0,962688 -0,847057 -2,464993 nx 0,0000 0,0000 -1,0000 v -0,962688 -0,847057 -2,636226 v -0,962688 0,691509 -2,636226 end begin_quad_strip nx 0,0000 0,0000 -1,0000 v -1,000000 -0,316414 2,025706 v 1,000000 -0,316414 2,025706 v -1,000000 -0,839104 2,025706 v 1,000000 -0,839104 2,025706 end");
			        GL.Enable(EnableCap.PolygonStipple);
 GL.Enable(EnableCap.PolygonStipple);
			        GL.PolygonStipple(halftone);
			          GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Aqua);
			          /*GL.Rotate(89.745d,1,0,0);			          
			          GL.Rotate(26.2489d,0,1,0);
			          GL.Rotate(-89.7075d,0,0,1);
			         Cube(-0.8f,6.0f,-3.4F,-1.396307f,0.2f,1.8f);
			        // GL.End();
			        */
			       // GL.Translate(0,10,10);
			       model("begin_quad_strip nx 0.0000 0,4091 0,9125 v -1,000000 0,701884 1,569230 v 1,000000 0,701884 1,569230 nx 0,0000 1,0000 0,0000 v -1,000000 -0,316414 2,025706 v 1,000000 -0,316414 2,025706 end");
			         GL.Disable(EnableCap.PolygonStipple);
			         

			        GL.PopMatrix();
			        
			     //   GL.PushMatrix();
			       // GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			 //       Cube(0,2,3,5,2,0.2f);
			        
			       // Cube(7,1,0,2,1.8f,0.2f);
			       GL.PushMatrix();
			       
			         GL.PopMatrix();
			         //GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Black);
			        /* Cube(0.8f,6.0f,-3.4F,-0.1f,0.2f,1.8f);
			        GL.PopMatrix();
			        GL.PushMatrix();
			        GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			        Cube(-0.4f,7.5f,0.4f,4.0f,0.1f,3.0f); 
			        GL.PopMatrix();
			        GL.PushMatrix();
			        GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			        Cube(0,2,-0.0f,5,2,0.2f);
			        GL.PopMatrix();
			        GL.PushMatrix();
			         GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			        Cube(0,5.5f,3,0.4f,2,0.2f);
			        GL.PopMatrix();
     GL.PushMatrix();
			         GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			        Cube(4-0.8f,5.5f,3,0.4f,2,0.2f);
			        GL.PopMatrix();
GL.PushMatrix();
			         GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			        Cube(-4-0.8f,5.5f,3,0.4f,2,0.2f);
			        GL.PopMatrix();			    
			                GL.PushMatrix();
			         GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			        Cube(0,5.5f,0,0.4f,2,0.2f);
			        GL.PopMatrix();
     GL.PushMatrix();
			         GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			        Cube(4-0.8f,5.5f,0,0.4f,2,0.2f);
			        GL.PopMatrix();
GL.PushMatrix();
			         GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Yellow);
			        Cube(-4-0.8f,5.5f,0,0.4f,2,0.2f);
			        GL.PopMatrix();	
			        */
			       GL.PushMatrix();
			       	model(debug_models);
			       	GL.PopMatrix();
			        GL.Material(MaterialFace.Front,MaterialParameter.Diffuse,Color.Green);  
			          }
				  glControl1.SwapBuffers();}
	
		
		void GlControl1Resize(object sender, EventArgs e)
		{
			               GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);

                 Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, Width / (float)Height, 1.0f, 64.0f);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projection);

  

		}
		
		void Timer1Tick(object sender, EventArgs e)
		{
			if(post_process == false){
			pictureBox1.Size = this.Size;	
		//	on_ground = false;
		pictureBox1.Invalidate();
		}
			if(fall2 == false){
				tra_y=tra_y+=0.01;
			}
					//value_j=0;
			
					
				
			
			
	
				
			if(on_ground == true){
			
				//stopupdate = true;
				
				
				fall2 = false;
					
			}
					
						
			if(on_ground == false){
			
				//stopupdate = true;
				
					
				fall2 = true;
			}
				//fall=true;
			//if(on_ground == true)
				//fall=false;
			if(jump == true){
				
				value_j++;
				tra_y-=gravity;
				if(value_j > 9){
					fall=true;
					jump=false;
				
					}
				
			}
			if(fall == true){
					value_j=0;
				val_++;
				if(val_ > 900){
					tra_y=0;
					fall=false;
					jump=false;
					val_=0;
				}
				tra_y+=gravity;
			}
			
			             if ( 
			            Convert.ToInt64(-tra_x) <= 1 + 0 && 10 + Convert.ToInt64(-tra_x) >= 0  &&
			            Convert.ToInt64(-tra_y+5) <= 1 + 0 && 10 + Convert.ToInt64(-tra_y+5) >= 0  &&
			            Convert.ToInt64(-tra_z+10) <= 1 + 0 && 10 + Convert.ToInt64(-tra_z+10) >= 0)
        
    {
					value_j=0;
				tra_y-=0.08d;
				fall=false;
				jump=false;
				on_ground = true;
    }

	  		             if ( 
			            Convert.ToInt64(-tra_x) <= 1 + 0 && 10 + Convert.ToInt64(-tra_x) >= 0  &&
			            Convert.ToInt64(-tra_y+5) <= 1 + 0 && 10 + Convert.ToInt64(-tra_y+5) >= 0  &&
			            Convert.ToInt64(-tra_z+10) <= 1 + 10 && 10 + Convert.ToInt64(-tra_z+10) >= 10)
        
    {
					value_j=0;
				tra_y-=0.08d;
				fall=false;
				jump=false;
				on_ground = true;
    }
			
		
					
			             if ( 
			            Convert.ToInt64(-tra_x) <= 1 + 17 && 10 + Convert.ToInt64(-tra_x) >= 17  &&
			            Convert.ToInt64(-tra_y+5) <= 1 + -2 && 10 + Convert.ToInt64(-tra_y+5) >= -2  &&
			            Convert.ToInt64(-tra_z+10) <= 1 + 0 && 10 + Convert.ToInt64(-tra_z+10) >= 0)
        
    {
					value_j=0;
				//	GL.ClearColor(Color.Gray);
				tra_y-=0.08d;
				fall=false;
				jump=false;
				on_ground = true;
    }
			
			
			glControl1.Invalidate();
		}
		
		void GlControl1KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.W)  {
				tra_z-=0.1d;
				
			}
			
					if (e.KeyCode == Keys.S)  {
				tra_z+=0.1d;
				
			}
			
					if (e.KeyCode == Keys.A)  {
				tra_x-=0.1d;
				
			}
					if (e.KeyCode == Keys.D)  {
				tra_x+=0.1d;
				
			}
			
			if(e.KeyCode == Keys.E)
				tra_y-=0.1d;
			
			if(e.KeyCode == Keys.Q)
				tra_y+=0.1d;
			if(e.KeyCode == Keys.Space){
				if(jump == false){
					if(fall == false){
						jump=true;
					}
				}
				
			}
				
		
		if(e.KeyCode == Keys.H){
				Console.WriteLine(tra_x.ToString());
			}
	
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
glControl1.Size = this.Size;

if(post_process == true){
	
	pictureBox1.Size = this.Size;
}

if(post_process == false){
	pictureBox1.Visible = false;
}

		}
		
		void MainFormResize(object sender, EventArgs e)
		{
			glControl1.Size = this.Size;
			pictureBox1.Size = this.Size;
		}
		
		void TextBox1TextChanged(object sender, EventArgs e)
		{
			hmmm = Int32.Parse(textBox1.Text);
		}
		
		void WalkTick(object sender, EventArgs e)
		{
			
			if(test_move == true){
				if(move_x < 5.0f){
				move_x+=0.1f;
			}
				
			}	
			//}
		
			
			if(test_move == false){
				move_x-=0.1f;
			}
			if(move_x > 5.0f){
				
				test_move=false;
				//Console.WriteLine(test_move.ToString());
			}
			
				if(move_x < -4.0f){
				test_move=true;
				//move_x=0;
				
			}
			
			/*animation_walk++;
			
			
			
			
			if(animation_walk == 1){
				r_leg = 10;
			}
			
				if(animation_walk == 4){
				r_leg = 15;
			}
				if(animation_walk == 8){
				r_leg = 20;
			}
				if(animation_walk == 12){
				r_leg = 25;
			}
				if(animation_walk == 16){
				
				r_leg = 30;
		
				
			}
				if(animation_walk == 20){
				
				r_leg = 25;
		
				
			}		
						if(animation_walk == 24){
				
				r_leg = 20;
		
				
			}	
						if(animation_walk == 28){
				
				r_leg = 15;
		
				
			}	
						if(animation_walk == 32){
				
				r_leg = 10;
		
				
			}	
						if(animation_walk == 36){
				
				r_leg = 5;
		
				
			}	
			if(animation_walk == 40){
				walk_char+=0.1f;
				animation_walk = 0;
		    }*/
		    
		}
		
		void Gravity2Tick(object sender, EventArgs e)
		{
tra_y=tra_y+=gravity;		
		}
		
	
		
		void WireframeSolidRenderToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(wireframe_solid == false){
				wireframe_solid=true;
			}else{
				wireframe_solid=false;
			}
			
		}
		
		void InstertObjectToolStripMenuItemClick(object sender, EventArgs e)
		{
			GL.PushMatrix();
			if(debugmdl == false){
			debug_models+=String.Format("box {0} {1} {2} 1 1 1",-tra_x,-tra_y+5,-tra_z+8);
			}else{
				debug_models+=String.Format(" box {0} {1} {2} 1 1 1",-tra_x,-tra_y+5,-tra_z+8);
			}
			debug_models = debug_models.Replace("  "," ");
			Console.WriteLine(debug_models);
		
			debugmdl = true;
			GL.PopMatrix();
		}
		
	

		
		
		    public static Bitmap Base64StringToBitmap(string base64String)
        {
            Bitmap bmpReturn = null;
            //Convert Base64 string to byte[]
            byte[] byteBuffer = Convert.FromBase64String(base64String);
            MemoryStream memoryStream = new MemoryStream(byteBuffer);

            memoryStream.Position = 0;

            bmpReturn = (Bitmap)Bitmap.FromStream(memoryStream);

            memoryStream.Close();
            memoryStream = null;
            byteBuffer = null;

            return bmpReturn;
        }
		    
		
		void PictureBox1Paint(object sender, PaintEventArgs e)
		{
		/*	    int w = this.ClientSize.Width;
    int h =this.ClientSize.Height;
    Bitmap bmp = new Bitmap(w, h);

   
    System.Drawing.Imaging.BitmapData data =
        bmp.LockBits(this.ClientRectangle, System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
    GL.ReadPixels(0, 0, this.ClientSize.Width, 7000, PixelFormat.Luminance, PixelType.UnsignedByte, data.Scan0);
    
    
    bmp.UnlockBits(data);*/
		if(post_process == false){
		   Bitmap bmp = new Bitmap(glControl1.Size.Width,glControl1.Size.Height);
		   
          //glControl1.Invalidate();
        int w = glControl1.ClientSize.Width;
        int h = glControl1.ClientSize.Height;
        
        
        string oof;
        
        
        bmp = new Bitmap(w, h);
        System.Drawing.Imaging.BitmapData data =
        bmp.LockBits(glControl1.ClientRectangle, 
        System.Drawing.Imaging.ImageLockMode.ReadWrite, 
        System.Drawing.Imaging.PixelFormat.Format24bppRgb);


        GL.ReadPixels(0, 0, w, h, OpenTK.Graphics.OpenGL.PixelFormat.Bgr, 
        PixelType.UnsignedByte, data.Scan0);
        bmp.UnlockBits(data);

        bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
			
        
        Graphics opengl = Graphics.FromImage(bmp);
        
        Brush red = new SolidBrush(Color.Red);
        //opengl.FillRectangle(red,0,0,20,20);
        
    
       // opengl.DrawImage(effect2,0,0);
          
			e.Graphics.DrawImage(bmp,0,0);
			
			bmp.Dispose();
		}
		
		}
		
	
	
		
		void TextureTick(object sender, EventArgs e)
		{
			
			   
			//floor_light.TranslateTransform(Convert.ToSingle(tra_x),Convert.ToSingle(tra_y));
		
			floor_light.Clear(Color.Black);
			
			
			floor_light.DrawImage(floor_texturetest,0,0);
			
			     floor_light.DrawImage(test_shadow,move_x*12,0);
			    
		//	GL.BindTexture(TextureTarget.Texture2D,tex);
		}
		
		void ShadowSystemToolStripMenuItemClick(object sender, EventArgs e)
		{
			//MessageBox.Show("Warning: If your PC is bad, this feature might cause your PC to turn off. Before using, save your work. This feature is experimental and will not be implemented fully into our game engine until we find a way to run it without any problems. This feature is sometimes enabled for short times in-game for giving realism in some rooms. By continuing, you are taking all risks.",MessageBoxButtons.YesNo,MessageBoxIcon.Stop,MessageBoxDefaultButton.Button1);
			if(ram_benchmark == false){
				ram_benchmark = true;
			}
				
			if(ram_benchmark == true){
				ram_benchmark = false;
			}
			
		}
	}
}
